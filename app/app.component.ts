// Importar el núcleo de Angular incluir el framework
import {Component} from 'angular2/core';
import {LoginComponent} from './components/login/login';
import {CNuevoComponent} from './components/cliente/cliente';
import {CBuscarComponent} from './components/cliente/buscar';
import {CEditarComponent} from './components/cliente/editar';

import {ROUTER_DIRECTIVES, RouteConfig, Router} from "angular2/router";

@Component({
	selector: 'system-app',
	templateUrl: "app/views/dashboard.html",
	directives : [CNuevoComponent,CBuscarComponent,CEditarComponent,ROUTER_DIRECTIVES],
	constructor() {
		// code...		
	}
})

@RouteConfig([
	{path: '/login', name: 'Login', component: LoginComponent},
	{path: '/cliente', name: 'Cnuevo', component: CNuevoComponent},
	{path: '/cliente/buscar', name: 'Cbuscar', component: CBuscarComponent},
	{path: '/cliente/editar', name: 'Ceditar', component: CEditarComponent}
	])



export class AppComponent {
	menu = false;

	constructor(private router: Router) {
		// code...
		console.log("componente principal");
			if(localStorage.getItem("user")){
				this.menu = true;
			}
			else{
				this.menu = false;
				this.router.navigate(['Login'])				
			}


	}
	logout(){
	console.log("Salir");
		localStorage.clear();		
		this.menu=false;
		window.location.href = 'http://localhost:8000/login';
	}
}