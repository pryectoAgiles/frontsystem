System.register(['angular2/core', './components/login/login', './components/cliente/cliente', './components/cliente/buscar', './components/cliente/editar', "angular2/router"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, login_1, cliente_1, buscar_1, editar_1, router_1;
    var AppComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (login_1_1) {
                login_1 = login_1_1;
            },
            function (cliente_1_1) {
                cliente_1 = cliente_1_1;
            },
            function (buscar_1_1) {
                buscar_1 = buscar_1_1;
            },
            function (editar_1_1) {
                editar_1 = editar_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            AppComponent = (function () {
                function AppComponent(router) {
                    this.router = router;
                    this.menu = false;
                    // code...
                    console.log("componente principal");
                    if (localStorage.getItem("user")) {
                        this.menu = true;
                    }
                    else {
                        this.menu = false;
                        this.router.navigate(['Login']);
                    }
                }
                AppComponent.prototype.logout = function () {
                    console.log("Salir");
                    localStorage.clear();
                    this.menu = false;
                    window.location.href = 'http://localhost:8000/login';
                };
                AppComponent = __decorate([
                    core_1.Component({
                        selector: 'system-app',
                        templateUrl: "app/views/dashboard.html",
                        directives: [cliente_1.CNuevoComponent, buscar_1.CBuscarComponent, editar_1.CEditarComponent, router_1.ROUTER_DIRECTIVES],
                        constructor: function () {
                            // code...		
                        }
                    }),
                    router_1.RouteConfig([
                        { path: '/login', name: 'Login', component: login_1.LoginComponent },
                        { path: '/cliente', name: 'Cnuevo', component: cliente_1.CNuevoComponent },
                        { path: '/cliente/buscar', name: 'Cbuscar', component: buscar_1.CBuscarComponent },
                        { path: '/cliente/editar', name: 'Ceditar', component: editar_1.CEditarComponent }
                    ]), 
                    __metadata('design:paramtypes', [router_1.Router])
                ], AppComponent);
                return AppComponent;
            }());
            exports_1("AppComponent", AppComponent);
        }
    }
});
//# sourceMappingURL=app.component.js.map