import {bootstrap}    from 'angular2/platform/browser'; //caragar el framework
import {ROUTER_PROVIDERS} from 'angular2/router';
import {HTTP_PROVIDERS, JSONP_PROVIDERS, Jsonp} from 'angular2/http';
import {AppComponent} from './app.component';

bootstrap(AppComponent, [ROUTER_PROVIDERS,HTTP_PROVIDERS,JSONP_PROVIDERS]); //cargar componente principal