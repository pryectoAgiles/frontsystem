// Importar el núcleo de Angular incluir el framework
import {Component} from 'angular2/core';
import {TipoPersonaService} from '../../services/catalogos/tipoPersona.service';
import {ClienteService} from '../../services/cliente/cliente.service';
import {Router, RouteParams} from "angular2/router";
import {Cliente} from '../../model/cliente';

@Component({
	selector: 'cliente-app',
	templateUrl: "app/views/clientes/cliente.html",
	providers: [TipoPersonaService, ClienteService]
})


export class CNuevoComponent {
	
	public razonSocial = new Array();
	public pais = new Array();
	public _id = this._id;
	public nombreRazonSocial = this.nombreRazonSocial;
	public tipoPersona = this.tipoPersona;
	public rfc = this.rfc;
	public calle = this.calle;
	public numExt = this.numExt;
	public numInt = this.numInt;
	public colonia = this.colonia;
	public delgMunicipio = this.delgMunicipio;
	public cp = this.cp;
	public referencias = this.referencias;
	public personaContacto = this.personaContacto;
	public telefono = this.telefono;
	public email = this.email;
	public clienteModel;
	public clientes : Cliente;
	public titulo = "";
	public estado = this.estado;


	constructor(private _TipoPersona: TipoPersonaService,private _ClienteService: ClienteService, private router: Router, params: RouteParams){
		this.titulo = "Alta";
		this.obtenerRazoSocial();
	}

	obtenerRazoSocial(){			
		this._TipoPersona.getTipoPersona().subscribe(data=>{
			console.log("Razon Social ",data.data)
				this.razonSocial = data.data;				
		},
		error=>{
			console.log(error);
		});			
	}		

	guardarCliente(form){
		
		this.clienteModel = new Cliente(
			this.nombreRazonSocial,
			this.tipoPersona,
			this.rfc,
			this.calle,
			this.numExt,
			this.numInt,
			this.colonia,
			this.delgMunicipio,
			this.cp,
			"1234",
			"2434",
			this.referencias,
			this.personaContacto,
			this.telefono,
			this.email,
			"1234");

		this._ClienteService.addCliente(this.clienteModel).subscribe(data=>{
			if(data.success){
				console.log("Cliente", data);
				//localStorage.setItem("user","true");				
				//this.router.ngOnDestroy();
        		//window.location.href = 'http://localhost:8000';
			}else{
				console.log("Error");
			}
		},
		error=>{
			console.log(error);
		});			
	}
	cmbEstado(value){
		console.log("Estado", value);
		this.pais=([{'id':"234","estado":"mex"},{'id':"345","estado":"est"}]);
		console.log("Pais", this.pais);
	}
}