// Importar el núcleo de Angular incluir el framework
import {Component} from 'angular2/core';
import {DropdownModule} from "ng2-dropdown";
import {TipoPersonaService} from '../../services/catalogos/tipoPersona.service';
import {ClienteService} from '../../services/cliente/cliente.service';
import {Router} from "angular2/router";
import {Cliente} from '../../model/cliente';


//import {Login} from './model/login';
//import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';
// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
// representan los metadatos
//TenplateUrl = perte guardar htl en un fichero html
@Component({
	selector: 'clientebuscar-app',
	templateUrl: "app/views/clientes/clienteBuscar.html",
	providers: [TipoPersonaService, ClienteService]
})


export class CBuscarComponent {
	
	public razonSocial = new Array();
	public clientes = new Array();
	public nombreRazonSocial;
	/*
	public nombreRazonSocial = this.nombreRazonSocial;
	public tipoPersona = this.tipoPersona;
	public rfc = this.rfc;
	public calle = this.calle;
	public numExt = this.numExt;
	public numInt = this.numInt;
	public colonia = this.colonia;
	public delgMunicipio = this.delgMunicipio;
	public cp = this.cp;
	public referencias = this.referencias;
	public personaContacto = this.personaContacto;
	public telefono = this.telefono;
	public email = this.email;*/
	public clienteModel : Cliente;
	//public nombreRazonSocial = this.nombreRazonSocial; 
	public correo = this.correo;
	public rfc = this.rfc;
	public contacto = this.contacto;

	constructor(private _TipoPersona: TipoPersonaService,private _ClienteService: ClienteService, private router: Router){
		//this.obtenerRazoSocial();
		//this.clienteModel = new Cliente(this.nombreRazonSocial);
		//console.log("Tipo Persona", this.tipoPersona);
			
	}

	obtenerRazoSocial(){			
		this._TipoPersona.getTipoPersona().subscribe(data=>{
			console.log(data)
				this.razonSocial = data.data;				
		},
		error=>{
			console.log(error);
		});			
	}
	getCliente(){
		this._ClienteService.getCliente(this.nombreRazonSocial, this.correo, this.rfc, this.contacto ).subscribe(data=>{			
				this.clientes = (data.success) ? data.data : [];				
		},
		error=>{
			console.log(error);
		});	
	}
	editarCliente(data){
		this.router.navigate(['Ceditar',{"id":data._id}]);
	}		


}