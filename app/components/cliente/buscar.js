System.register(['angular2/core', '../../services/catalogos/tipoPersona.service', '../../services/cliente/cliente.service', "angular2/router"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, tipoPersona_service_1, cliente_service_1, router_1;
    var CBuscarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (tipoPersona_service_1_1) {
                tipoPersona_service_1 = tipoPersona_service_1_1;
            },
            function (cliente_service_1_1) {
                cliente_service_1 = cliente_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            }],
        execute: function() {
            //import {Login} from './model/login';
            //import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';
            // Decorador component, indicamos en que etiqueta se va a cargar la plantilla
            // representan los metadatos
            //TenplateUrl = perte guardar htl en un fichero html
            CBuscarComponent = (function () {
                function CBuscarComponent(_TipoPersona, _ClienteService, router) {
                    //this.obtenerRazoSocial();
                    //this.clienteModel = new Cliente(this.nombreRazonSocial);
                    //console.log("Tipo Persona", this.tipoPersona);
                    this._TipoPersona = _TipoPersona;
                    this._ClienteService = _ClienteService;
                    this.router = router;
                    this.razonSocial = new Array();
                    this.clientes = new Array();
                    //public nombreRazonSocial = this.nombreRazonSocial; 
                    this.correo = this.correo;
                    this.rfc = this.rfc;
                    this.contacto = this.contacto;
                }
                CBuscarComponent.prototype.obtenerRazoSocial = function () {
                    var _this = this;
                    this._TipoPersona.getTipoPersona().subscribe(function (data) {
                        console.log(data);
                        _this.razonSocial = data.data;
                    }, function (error) {
                        console.log(error);
                    });
                };
                CBuscarComponent.prototype.getCliente = function () {
                    var _this = this;
                    this._ClienteService.getCliente(this.nombreRazonSocial, this.correo, this.rfc, this.contacto).subscribe(function (data) {
                        _this.clientes = (data.success) ? data.data : [];
                    }, function (error) {
                        console.log(error);
                    });
                };
                CBuscarComponent.prototype.editarCliente = function (data) {
                    this.router.navigate(['Ceditar', { "id": data._id }]);
                };
                CBuscarComponent = __decorate([
                    core_1.Component({
                        selector: 'clientebuscar-app',
                        templateUrl: "app/views/clientes/clienteBuscar.html",
                        providers: [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService]
                    }), 
                    __metadata('design:paramtypes', [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService, router_1.Router])
                ], CBuscarComponent);
                return CBuscarComponent;
            }());
            exports_1("CBuscarComponent", CBuscarComponent);
        }
    }
});
//# sourceMappingURL=buscar.js.map