System.register(['angular2/core', '../../services/catalogos/tipoPersona.service', '../../services/cliente/cliente.service', "angular2/router", '../../model/cliente'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, tipoPersona_service_1, cliente_service_1, router_1, cliente_1;
    var CNuevoComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (tipoPersona_service_1_1) {
                tipoPersona_service_1 = tipoPersona_service_1_1;
            },
            function (cliente_service_1_1) {
                cliente_service_1 = cliente_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (cliente_1_1) {
                cliente_1 = cliente_1_1;
            }],
        execute: function() {
            CNuevoComponent = (function () {
                function CNuevoComponent(_TipoPersona, _ClienteService, router, params) {
                    this._TipoPersona = _TipoPersona;
                    this._ClienteService = _ClienteService;
                    this.router = router;
                    this.razonSocial = new Array();
                    this.pais = new Array();
                    this._id = this._id;
                    this.nombreRazonSocial = this.nombreRazonSocial;
                    this.tipoPersona = this.tipoPersona;
                    this.rfc = this.rfc;
                    this.calle = this.calle;
                    this.numExt = this.numExt;
                    this.numInt = this.numInt;
                    this.colonia = this.colonia;
                    this.delgMunicipio = this.delgMunicipio;
                    this.cp = this.cp;
                    this.referencias = this.referencias;
                    this.personaContacto = this.personaContacto;
                    this.telefono = this.telefono;
                    this.email = this.email;
                    this.titulo = "";
                    this.estado = this.estado;
                    this.titulo = "Alta";
                    this.obtenerRazoSocial();
                }
                CNuevoComponent.prototype.obtenerRazoSocial = function () {
                    var _this = this;
                    this._TipoPersona.getTipoPersona().subscribe(function (data) {
                        console.log("Razon Social ", data.data);
                        _this.razonSocial = data.data;
                    }, function (error) {
                        console.log(error);
                    });
                };
                CNuevoComponent.prototype.guardarCliente = function (form) {
                    this.clienteModel = new cliente_1.Cliente(this.nombreRazonSocial, this.tipoPersona, this.rfc, this.calle, this.numExt, this.numInt, this.colonia, this.delgMunicipio, this.cp, "1234", "2434", this.referencias, this.personaContacto, this.telefono, this.email, "1234");
                    this._ClienteService.addCliente(this.clienteModel).subscribe(function (data) {
                        if (data.success) {
                            console.log("Cliente", data);
                        }
                        else {
                            console.log("Error");
                        }
                    }, function (error) {
                        console.log(error);
                    });
                };
                CNuevoComponent.prototype.cmbEstado = function (value) {
                    console.log("Estado", value);
                    this.pais = ([{ 'id': "234", "estado": "mex" }, { 'id': "345", "estado": "est" }]);
                    console.log("Pais", this.pais);
                };
                CNuevoComponent = __decorate([
                    core_1.Component({
                        selector: 'cliente-app',
                        templateUrl: "app/views/clientes/cliente.html",
                        providers: [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService]
                    }), 
                    __metadata('design:paramtypes', [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService, router_1.Router, router_1.RouteParams])
                ], CNuevoComponent);
                return CNuevoComponent;
            }());
            exports_1("CNuevoComponent", CNuevoComponent);
        }
    }
});
//# sourceMappingURL=cliente.js.map