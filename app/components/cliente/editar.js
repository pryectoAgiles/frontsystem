System.register(['angular2/core', '../../services/catalogos/tipoPersona.service', '../../services/cliente/cliente.service', "angular2/router", '../../model/cliente'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, tipoPersona_service_1, cliente_service_1, router_1, cliente_1;
    var CEditarComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (tipoPersona_service_1_1) {
                tipoPersona_service_1 = tipoPersona_service_1_1;
            },
            function (cliente_service_1_1) {
                cliente_service_1 = cliente_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (cliente_1_1) {
                cliente_1 = cliente_1_1;
            }],
        execute: function() {
            CEditarComponent = (function () {
                function CEditarComponent(_TipoPersona, _ClienteService, router, params) {
                    this._TipoPersona = _TipoPersona;
                    this._ClienteService = _ClienteService;
                    this.router = router;
                    this.razonSocial = new Array();
                    this.nombreRazonSocial = this.nombreRazonSocial;
                    this.tipoPersona = this.tipoPersona;
                    this.rfc = this.rfc;
                    this.calle = this.calle;
                    this.numExt = this.numExt;
                    this.numInt = this.numInt;
                    this.colonia = this.colonia;
                    this.delgMunicipio = this.delgMunicipio;
                    this.cp = this.cp;
                    this.referencias = this.referencias;
                    this.personaContacto = this.personaContacto;
                    this.telefono = this.telefono;
                    this.email = this.email;
                    this.titulo = "";
                    this.titulo = "Editar";
                    this.obtenerRazoSocial();
                    this.obtenerCliente(params.get("id"));
                }
                CEditarComponent.prototype.obtenerCliente = function (id) {
                    var _this = this;
                    this._ClienteService.getClienteId(id).subscribe(function (data) {
                        var datos = data.data;
                        var direccion = data.data.direccion;
                        var cd = data.data.direccion.ciudad;
                        if (data.success) {
                            _this.nombreRazonSocial = datos.nombreRazonSocial;
                            _this.tipoPersona = datos.tipoPersona._id;
                            _this.rfc = datos.rfc;
                            _this.calle = direccion.calle;
                            _this.numExt = direccion.numExt;
                            _this.numInt = direccion.numInt;
                            _this.colonia = direccion.colonia;
                            _this.delgMunicipio = direccion.delgMunicipio;
                            _this.cp = direccion.cp;
                            _this.referencias = direccion.referencias;
                            _this.personaContacto = datos.personaContacto;
                            _this.telefono = datos.telefono;
                            _this.email = datos.email;
                        }
                    }, function (error) {
                        console.log(error);
                    });
                };
                CEditarComponent.prototype.obtenerRazoSocial = function () {
                    var _this = this;
                    this._TipoPersona.getTipoPersona().subscribe(function (data) {
                        _this.razonSocial = data.data;
                    }, function (error) {
                        console.log(error);
                    });
                };
                CEditarComponent.prototype.guardarCliente = function (form) {
                    this.clienteModel = new cliente_1.Cliente(this.nombreRazonSocial, this.tipoPersona, this.rfc, this.calle, this.numExt, this.numInt, this.colonia, this.delgMunicipio, this.cp, "1234", "2434", this.referencias, this.personaContacto, this.telefono, this.email, "1234");
                    console.log(this.clienteModel);
                    /*this._ClienteService.addCliente(this.clienteModel).subscribe(data=>{
                        if(data.success){
                            console.log("Cliente", data);
                            //localStorage.setItem("user","true");
                            //this.router.ngOnDestroy();
                            //window.location.href = 'http://localhost:8000';
                        }else{
                            console.log("Error");
                        }
                    },
                    error=>{
                        console.log(error);
                    });	*/
                };
                CEditarComponent = __decorate([
                    core_1.Component({
                        selector: 'cliente-app',
                        templateUrl: "app/views/clientes/cliente.html",
                        providers: [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService]
                    }), 
                    __metadata('design:paramtypes', [tipoPersona_service_1.TipoPersonaService, cliente_service_1.ClienteService, router_1.Router, router_1.RouteParams])
                ], CEditarComponent);
                return CEditarComponent;
            }());
            exports_1("CEditarComponent", CEditarComponent);
        }
    }
});
//# sourceMappingURL=editar.js.map