// Importar el núcleo de Angular incluir el framework
import {Component} from 'angular2/core';
import {TipoPersonaService} from '../../services/catalogos/tipoPersona.service';
import {ClienteService} from '../../services/cliente/cliente.service';
import {Router, RouteParams} from "angular2/router";
import {Cliente} from '../../model/cliente';

@Component({
	selector: 'cliente-app',
	templateUrl: "app/views/clientes/cliente.html",
	providers: [TipoPersonaService, ClienteService]
})

export class CEditarComponent {
	
	public razonSocial = new Array();

	
	public nombreRazonSocial = this.nombreRazonSocial;
	public tipoPersona = this.tipoPersona;
	public rfc = this.rfc;
	public calle = this.calle;
	public numExt = this.numExt;
	public numInt = this.numInt;
	public colonia = this.colonia;
	public delgMunicipio = this.delgMunicipio;
	public cp = this.cp;
	public referencias = this.referencias;
	public personaContacto = this.personaContacto;
	public telefono = this.telefono;
	public email = this.email;
	public titulo = "";
	public clienteModel;
	public clientes : Cliente;


	constructor(private _TipoPersona: TipoPersonaService,private _ClienteService: ClienteService, private router: Router, params: RouteParams){
		this.titulo = "Editar";
		this.obtenerRazoSocial();	
		this.obtenerCliente(params.get("id"));		
	}
	obtenerCliente(id){
		this._ClienteService.getClienteId(id).subscribe(data=>{
		let datos = data.data;			
		let direccion = data.data.direccion;
		let cd = data.data.direccion.ciudad;			
			if(data.success){
				this.nombreRazonSocial = datos.nombreRazonSocial;
				this.tipoPersona = datos.tipoPersona._id;
				this.rfc = datos.rfc;
				this.calle = direccion.calle;
				this.numExt = direccion.numExt;
				this.numInt = direccion.numInt;
				this.colonia = direccion.colonia;
				this.delgMunicipio = direccion.delgMunicipio;
				this.cp = direccion.cp;
				this.referencias = direccion.referencias;
				this.personaContacto = datos.personaContacto;
				this.telefono = datos.telefono;
				this.email = datos.email;				
				//this.
			}
		},
		error=>{
			console.log(error);
		});			

	}
	obtenerRazoSocial(){			
		this._TipoPersona.getTipoPersona().subscribe(data=>{
				this.razonSocial = data.data;				
		},
		error=>{
			console.log(error);
		});			
	},
	guardarCliente(form){
		
		this.clienteModel = new Cliente(
			this.nombreRazonSocial,
			this.tipoPersona,
			this.rfc,
			this.calle,
			this.numExt,
			this.numInt,
			this.colonia,
			this.delgMunicipio,
			this.cp,
			"1234",
			"2434",
			this.referencias,
			this.personaContacto,
			this.telefono,
			this.email,
			"1234");

		console.log(this.clienteModel);

		/*this._ClienteService.addCliente(this.clienteModel).subscribe(data=>{
			if(data.success){
				console.log("Cliente", data);
				//localStorage.setItem("user","true");				
				//this.router.ngOnDestroy();
        		//window.location.href = 'http://localhost:8000';
			}else{
				console.log("Error");
			}
		},
		error=>{
			console.log(error);
		});	*/		
	}
}		

}