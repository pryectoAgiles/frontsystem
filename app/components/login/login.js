System.register(['angular2/core', '../../services/login/login.service', "angular2/router", '../../model/login'], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, login_service_1, router_1, login_1;
    var LoginComponent;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (login_service_1_1) {
                login_service_1 = login_service_1_1;
            },
            function (router_1_1) {
                router_1 = router_1_1;
            },
            function (login_1_1) {
                login_1 = login_1_1;
            }],
        execute: function() {
            //import {Login} from './model/login';
            //import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';
            // Decorador component, indicamos en que etiqueta se va a cargar la plantilla
            // representan los metadatos
            //TenplateUrl = perte guardar htl en un fichero html
            LoginComponent = (function () {
                function LoginComponent(_LoginService, router) {
                    this._LoginService = _LoginService;
                    this.router = router;
                    this.usuario = this.usuario;
                    this.contrasena = this.contrasena;
                    this.datos = [];
                }
                LoginComponent.prototype.onSubmit = function () {
                    this.loginModel = new login_1.Login(this.usuario, this.contrasena);
                    this._LoginService.getValidaUsuario(this.loginModel).subscribe(function (data) {
                        if (data.success) {
                            console.log("Exito");
                            localStorage.setItem("user", "true");
                            //this.router.ngOnDestroy();
                            window.location.href = 'http://localhost:8000';
                        }
                        else {
                            console.log("Error");
                        }
                    }, function (error) {
                        console.log(error);
                    });
                    //console.log("Obteniendo datos ",this.datos);
                };
                LoginComponent.prototype.iniciaSession = function () {
                };
                LoginComponent = __decorate([
                    core_1.Component({
                        templateUrl: "app/views/login/login.html",
                        providers: [login_service_1.LoginService]
                    }), 
                    __metadata('design:paramtypes', [login_service_1.LoginService, router_1.Router])
                ], LoginComponent);
                return LoginComponent;
            }());
            exports_1("LoginComponent", LoginComponent);
        }
    }
});
//# sourceMappingURL=login.js.map