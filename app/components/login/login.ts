// Importar el núcleo de Angular incluir el framework
import {Component} from 'angular2/core';
import {LoginService} from '../../services/login/login.service';
import {Router} from "angular2/router";
//OBTENER UN MODELO
import {Login} from '../../model/login';

//import {Login} from './model/login';
//import { ButtonsModule } from 'ng2-bootstrap/ng2-bootstrap';
// Decorador component, indicamos en que etiqueta se va a cargar la plantilla
// representan los metadatos
//TenplateUrl = perte guardar htl en un fichero html
@Component({	
	templateUrl: "app/views/login/login.html",
	providers: [LoginService]
})

export class LoginComponent {
	public usuario = this.usuario;
	public contrasena = this.contrasena;
	public loginModel;
	public datos = [];

	constructor(private _LoginService: LoginService, private router: Router){}

	onSubmit(){	
		this.loginModel = new Login(this.usuario,this.contrasena);
		this._LoginService.getValidaUsuario(this.loginModel).subscribe(data=>{
			if(data.success){
				console.log("Exito");
				localStorage.setItem("user","true");				
				//this.router.ngOnDestroy();
        		window.location.href = 'http://localhost:8000';
			}else{
				console.log("Error");
			}
		},
		error=>{
			console.log(error);
		});			

		//console.log("Obteniendo datos ",this.datos);
	}		

	iniciaSession(){

	}
}