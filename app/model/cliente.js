System.register([], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var Cliente;
    return {
        setters:[],
        execute: function() {
            Cliente = (function () {
                function Cliente(nombreRazoSocial, tipoPersona, rfc, calle, numExt, numInt, colinia, delgMunicipio, cp, idEstado, idPais, referencias, personaContacto, telefono, email, usuarioAlta) {
                    this.nombreRazoSocial = nombreRazoSocial;
                    this.tipoPersona = tipoPersona;
                    this.rfc = rfc;
                    this.calle = calle;
                    this.numExt = numExt;
                    this.numInt = numInt;
                    this.colinia = colinia;
                    this.delgMunicipio = delgMunicipio;
                    this.cp = cp;
                    this.idEstado = idEstado;
                    this.idPais = idPais;
                    this.referencias = referencias;
                    this.personaContacto = personaContacto;
                    this.telefono = telefono;
                    this.email = email;
                    this.usuarioAlta = usuarioAlta;
                }
                return Cliente;
            }());
            exports_1("Cliente", Cliente);
        }
    }
});
//# sourceMappingURL=cliente.js.map