export class Cliente{
	constructor(
		public  nombreRazoSocial: string,
		public tipoPersona: string,
		public rfc : string,
		public calle: string,
		public numExt: number,
		public numInt: number,
		public colinia: string,
		public delgMunicipio: string,
		public cp: number,
		public idEstado : string,
		public idPais : string,
		public referencias : string,
		public personaContacto : string,
		public telefono : number,
		public email : string,
		public usuarioAlta : string		
		){}
}