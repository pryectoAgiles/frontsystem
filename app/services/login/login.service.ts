import {Injectable} from "angular2/core";
import {Response,Http,JSONP_PROVIDERS,Jsonp,URLSearchParams} from "angular2/http";
import {Headers, RequestOptions } from 'angular2/http';

import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Login} from "../../model/login";


@Injectable()
export class LoginService{
	constructor(private _http: Http){}

  getValidaUsuario(login){
  	let bodyString = "usuario="+login.usuario+"&contrasena="+login.contrasena;
  	let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers });
		return this._http.post("http://localhost:3000/accesoUsuario/",bodyString , options).map((res) => res.json(), (err) => err.json());
	}
}