import {Injectable} from "angular2/core";
import {Response,Http,JSONP_PROVIDERS,Jsonp,URLSearchParams} from "angular2/http";
import {Headers, RequestOptions } from 'angular2/http';

import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";


@Injectable()
export class TipoPersonaService{
	constructor(private _http: Http){}

  getTipoPersona(){  
  	let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers });
		return this._http.get("http://localhost:3000/razonsocial/").map((res) => res.json(), (err) => err.json());
	}
}