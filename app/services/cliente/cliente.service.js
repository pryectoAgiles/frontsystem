System.register(["angular2/core", "angular2/http", "rxjs/add/operator/map"], function(exports_1, context_1) {
    "use strict";
    var __moduleName = context_1 && context_1.id;
    var __decorate = (this && this.__decorate) || function (decorators, target, key, desc) {
        var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
        if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
        else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
        return c > 3 && r && Object.defineProperty(target, key, r), r;
    };
    var __metadata = (this && this.__metadata) || function (k, v) {
        if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
    };
    var core_1, http_1, http_2;
    var ClienteService;
    return {
        setters:[
            function (core_1_1) {
                core_1 = core_1_1;
            },
            function (http_1_1) {
                http_1 = http_1_1;
                http_2 = http_1_1;
            },
            function (_1) {}],
        execute: function() {
            ClienteService = (function () {
                function ClienteService(_http) {
                    this._http = _http;
                }
                ClienteService.prototype.addCliente = function (cliente) {
                    var array = [];
                    array["direccion"] = cliente.calle;
                    var bodyString = "nombreRazonSocial=" + cliente.nombreRazoSocial +
                        "&tipoPersona=" + cliente.tipoPersona +
                        "&rfc=" + cliente.rfc +
                        "&direccion[calle]=" + cliente.calle +
                        "&direccion[numExt]=" + cliente.numExt +
                        "&direccion[numInt]=" + cliente.numInt +
                        "&direccion[colonia]=" + cliente.colonia +
                        "&direccion[delgMunicipio]=" + cliente.delgMunicipio +
                        "&direccion[cp]=" + cliente.cp +
                        "&direccion[ciudad][idEstado]=" + cliente.idEstado +
                        "&direccion[ciudad][idPais]=" + cliente.idPais +
                        "&direccion[ciudad][referencias]=" + cliente.referencias +
                        "&personaContacto=" + cliente.personaContacto +
                        "&telefono=" + cliente.telefono +
                        "&email=" + cliente.email +
                        "&usuarioAlta=" + cliente.usuarioAlta;
                    var headers = new http_2.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
                    var options = new http_2.RequestOptions({ headers: headers });
                    return this._http.post("http://localhost:3000/cliente/", bodyString, options).map(function (res) { return res.json(); }, function (err) { return err.json(); });
                };
                ClienteService.prototype.getCliente = function (nombreRS, correo, rfc, contacto) {
                    console.log(nombreRS, correo, rfc, contacto);
                    nombreRS = (nombreRS !== undefined && nombreRS !== '') ? nombreRS = nombreRS : null;
                    correo = (correo !== undefined && correo !== '') ? correo = correo : null;
                    rfc = (rfc !== undefined && rfc !== '') ? rfc = rfc : null;
                    contacto = (contacto !== undefined && contacto !== '') ? contacto = contacto : null;
                    console.log(nombreRS, correo, rfc, contacto);
                    var headers = new http_2.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
                    var options = new http_2.RequestOptions({ headers: headers });
                    return this._http.get("http://localhost:3000/clienteFilter/" + nombreRS + "/" + rfc + "/" + contacto + "/" + correo + "/null", options).map(function (res) { return res.json(); }, function (err) { return err.json(); });
                };
                ClienteService.prototype.getClienteId = function (id) {
                    var headers = new http_2.Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
                    var options = new http_2.RequestOptions({ headers: headers });
                    return this._http.get("http://localhost:3000/cliente/" + id, options).map(function (res) { return res.json(); }, function (err) { return err.json(); });
                };
                ClienteService = __decorate([
                    core_1.Injectable(), 
                    __metadata('design:paramtypes', [http_1.Http])
                ], ClienteService);
                return ClienteService;
            }());
            exports_1("ClienteService", ClienteService);
        }
    }
});
//# sourceMappingURL=cliente.service.js.map