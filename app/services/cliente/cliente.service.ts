import {Injectable} from "angular2/core";
import {Response,Http,JSONP_PROVIDERS,Jsonp,URLSearchParams} from "angular2/http";
import {Headers, RequestOptions } from 'angular2/http';
import "rxjs/add/operator/map";
import {Observable} from "rxjs/Observable";
import {Login} from "../../model/login";

@Injectable()
export class ClienteService{
	constructor(private _http: Http){}

  addCliente(cliente){

  	var array = [];
  	array["direccion"]=cliente.calle;
  	
  	let bodyString = 
  	"nombreRazonSocial="+cliente.nombreRazoSocial+
  	"&tipoPersona="+cliente.tipoPersona+
  	"&rfc="+cliente.rfc+
  	"&direccion[calle]="+cliente.calle+
  	"&direccion[numExt]="+cliente.numExt+
  	"&direccion[numInt]="+cliente.numInt+
  	"&direccion[colonia]="+cliente.colonia+
  	"&direccion[delgMunicipio]="+cliente.delgMunicipio+
  	"&direccion[cp]="+cliente.cp+
  	"&direccion[ciudad][idEstado]="+cliente.idEstado+
  	"&direccion[ciudad][idPais]="+cliente.idPais+
  	"&direccion[ciudad][referencias]="+cliente.referencias+
  	"&personaContacto="+cliente.personaContacto+
  	"&telefono="+cliente.telefono+
  	"&email="+cliente.email+
  	"&usuarioAlta="+cliente.usuarioAlta;

  	let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers });
	return this._http.post("http://localhost:3000/cliente/",bodyString , options).map((res) => res.json(), (err) => err.json());
	}
  getCliente(nombreRS,correo,rfc,contacto){
    console.log(nombreRS,correo,rfc,contacto);

  nombreRS = (nombreRS !== undefined && nombreRS !== '') ? nombreRS = nombreRS : null;
  correo = (correo !== undefined && correo !== '') ? correo = correo : null;
  rfc = (rfc !== undefined && rfc !== '') ? rfc = rfc : null;
  contacto = (contacto !== undefined && contacto !== '') ? contacto = contacto : null;
  

    console.log(nombreRS,correo,rfc,contacto);

    let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers });
  return this._http.get("http://localhost:3000/clienteFilter/"+nombreRS+"/"+rfc+"/"+contacto+"/"+correo+"/null",options).map((res) => res.json(), (err) => err.json());
  }
  getClienteId(id){
    let headers      = new Headers({ 'Content-Type': 'application/x-www-form-urlencoded' }); // ... Set content type to JSON
    let options       = new RequestOptions({ headers: headers });
  return this._http.get("http://localhost:3000/cliente/"+id,options).map((res) => res.json(), (err) => err.json());
  }

}	